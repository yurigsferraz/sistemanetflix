package bd.SistemaNetflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaNetflixApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaNetflixApplication.class, args);
	}

}
