package bd.SistemaNetflix.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class SistemaNetflixRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<ProdutoDTO> recuperarProdutosComValorMaior(Float valor)
    {
        String sql = " SELECT p.descricao AS produto, p.unidade AS unidade, p.valor_unitario AS valorUnitario " +
                " FROM Produto p " +
                " WHERE p.valor_unitario >= :valor";

        return jdbcTemplate.queryForStream(sql, Map.of("valor", valor), BeanPropertyRowMapper.newInstance(ProdutoDTO.class)).toList();



    }
