package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieDTO {

    private ContentDTO contentDTO;
    private int minutes;
}
