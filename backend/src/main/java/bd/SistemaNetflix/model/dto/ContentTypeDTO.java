package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContentTypeDTO {
    private ContentDTO contentDTO;
    private TypeDTO typeDTO;
}
