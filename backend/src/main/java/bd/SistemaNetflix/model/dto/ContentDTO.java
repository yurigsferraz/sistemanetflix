package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ContentDTO {

    private String title;
    private String description;
    private String rating;
    private Date dateAdded;
}
