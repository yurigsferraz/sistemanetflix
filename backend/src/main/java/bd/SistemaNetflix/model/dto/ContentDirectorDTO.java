package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContentDirectorDTO {
    private ContentDTO contentDTO;
    private DirectorDTO directorDTO;
}
