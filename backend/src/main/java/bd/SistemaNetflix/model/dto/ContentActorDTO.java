package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContentActorDTO {
    private ContentDTO contentDTO;
    private ActorDTO actorDTO;
}
