package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TVShowDTO {

    private ContentDTO contentDTO;
    private int seasons;
}
