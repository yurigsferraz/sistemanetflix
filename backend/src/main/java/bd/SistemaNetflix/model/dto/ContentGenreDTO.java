package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContentGenreDTO {
    private ContentDTO contentDTO;
    private GenreDTO genreDTO;
}
