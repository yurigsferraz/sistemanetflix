package bd.SistemaNetflix.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DirectorDTO {

    private String name;
}
